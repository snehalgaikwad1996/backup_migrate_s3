# Backup and Migrate for Drupal 8

Backup the Drupal database and files or migrate them to AWS S3.

## Installation

This module uses composer to manage dependencies.

 To upload backup on AWS S3 :

 1. Add S3 destination : /admin/config/development/backup_migrate/settings/destination. Select Amazon S3

 2. Enter details needed to create S3 client.

 3. Store secret access key and access id in settings.php.
    $settings['backup_migrate_aws_secret_key'] = 'secret_key';
    $settings['backup_migrate_aws_access_key'] = 'key';

 4. Now you can select backup destination just created to store backup.

 Note: To use AWS S3 as a backup storage, this module requires `aws-sdk-php` library. So make sure this library is present.

For more information on using composer see: https://getcomposer.org/
